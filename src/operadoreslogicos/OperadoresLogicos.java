package operadoreslogicos;

public class OperadoresLogicos {

	public static void main(String[] args) {
		int dezesseteAnos = 17;
		int dezoitoAnos = 18;
		String primeiraCondicaoe = ("Joana n�o tem 18 anos e tem 250 reais.");
		String segundaCondicaoe = ("Joana tem 18 anos e n�o tem 250 reais.");
		String terceiraCondicaoe = ("Joana tem 18 anos e tem 250 reais.");
		String quartaCondicaoou = ("Joana n�o tem 18 anos e n�o tem 250 reais.");
	
		float duzentosECinquenta = 250;
		
		System.out.println(primeiraCondicaoe + (dezesseteAnos >= 18 && duzentosECinquenta >= 250));
		System.out.println(segundaCondicaoe + (dezoitoAnos >= 18 && duzentosECinquenta <=170));
		System.out.println(terceiraCondicaoe + (dezoitoAnos >= 18 && duzentosECinquenta >= 250));
		System.out.println("\n");
		System.out.println(primeiraCondicaoe + (dezesseteAnos <= 18 || duzentosECinquenta >= 250));
		System.out.println(segundaCondicaoe + (dezoitoAnos >= 18 || duzentosECinquenta <= 170));
		System.out.println(quartaCondicaoou + (dezesseteAnos >= 18 || duzentosECinquenta <= 100));
	}

}
