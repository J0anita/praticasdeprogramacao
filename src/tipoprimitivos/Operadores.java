package tipoprimitivos;

public class Operadores {

	public static void main(String[] args) {
		String nome = "Joana";
		String salario = "5000";
		String sexo = "F";
		String idade = "25";
		String estadoCivil = "Solteira";
		
		System.out.println("O(A) trabalhador(a) "+ nome + ", do sexo " + sexo + ", idade, " + idade + ", estado civil " + estadoCivil +
				", e salario " + salario + " encontra-se empregado neste estabelecimento.");

	}

}
